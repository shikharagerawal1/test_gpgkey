package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

func main() {
	//lists the name of file
	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		fmt.Println(file.Name())
	}

	//enter name of file you want to sign

	var fileToSign string
	fmt.Scanln(&fileToSign)
	println("file name is", fileToSign)

	//check weather file name is existed or not
	var flag int
	for _, file := range files {
		if fileToSign == file.Name() {
			flag = 1
			break
		}
	}
	println("file exist", flag) //0 if not exist, 1 if exist
	//----for running [gpg --sign filename] command
	path, _ := os.Getwd()
	// println("Current Path", path)
	cmd := exec.Command("gpg", "--sign", fileToSign)
	cmd.Dir = path
	if err := cmd.Run(); err != nil {
		fmt.Println(err)
	}
	println("------------------verification--------")
	//for file verification
	var fileToValidate string
	fmt.Scanln(&fileToValidate)
	println("name of file to validate is", fileToValidate)
	// var outb, errb bytes.Buffer
	cmd = exec.Command("gpg", "--verify", fileToValidate)
	cmd.Dir = path
	// cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		fmt.Println(err)
	}
}
